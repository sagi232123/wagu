﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.IO;

namespace Wagu
{
    public partial class Server : Form
    {
        public DataTable runQuery(string query)
        {
            //TODO: try & catch
           var myConnectionString = "server=127.0.0.1;uid=root;" +
                "pwd=;database=waguu;";
            var conn = new MySql.Data.MySqlClient.MySqlConnection();
            try
            {
    
                conn.ConnectionString = myConnectionString;
             
                conn.Open();
                MessageBox.Show("Connected sql");
            }
                
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                
                MessageBox.Show(ex.Message);
            }
            
            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(query, conn);
            MySqlDataAdapter returnVal = new MySqlDataAdapter(query, conn);
            DataTable dt = new DataTable("CharacterInfo");
            returnVal.Fill(dt);
            return dt;
        }
        public DataTable getSeriesList()
        {
            string query = "SELECT * FROM series";
            return runQuery(query);
        }
        public DataTable getMoviesList()
        {
            string query = "SELECT * FROM movies";
            return runQuery(query);
        }
        public DataTable getMoviesComedyList()
        {
            string query = "SELECT * FROM movies WHERE type = 1";
            return runQuery(query);
        }
        public DataTable getMoviesActionList()
        {
            string query = "SELECT * FROM movies WHERE type  = 2 ";
            return runQuery(query);
        }
        
        public DataTable getMoviesDramaList()
        {
            string query = "SELECT * FROM movies WHERE type = 3";
            return runQuery(query);
        }
        public DataTable getMoviesHorrorList()
        {
            string query = "SELECT * FROM movies WNERE type = 4";
            return runQuery(query);
        }
        public Server()
        {
            InitializeComponent();
                ipAd = IPAddress.Parse("127.0.0.1"); //use local m/c IP address, and use the same in the client
                /* Initializes the Listener */
                myList = new TcpListener(ipAd, 8001);
                /* Start Listeneting at the specified port */
                myList.Start();
                tcpThd = new Thread(new ThreadStart(Waiting));
                tcpThd.Start();

                
        }
        private int port=8001;
        private TcpListener myList;
        private IPAddress ipAd;
        private Thread tcpThd;
        MySqlDataAdapter pb;
        private void Server_Load(object sender, EventArgs e)
        {
            //Starting Server:
            lblip.Text = ipAd.ToString() + ":" + port.ToString();
            //

            //MySql.Data.MySqlClient.MySqlConnection conn;
            //string myConnectionString;

            //myConnectionString = "server=127.0.0.1;uid=root;" +
            //    "pwd=;database=waguu;";
            //conn = new MySql.Data.MySqlClient.MySqlConnection();
            //try
            //{
    
            //    conn.ConnectionString = myConnectionString;
             
            //    conn.Open();
            //    MessageBox.Show("Connected sql");
            //}
                
            //catch (MySql.Data.MySqlClient.MySqlException ex)
            //{
                
            //    MessageBox.Show(ex.Message);
            //}
            //string query = "select * from movies";
            //MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(query, conn);
            //MySqlDataAdapter returnVal = new MySqlDataAdapter(query, conn);
            //DataTable dt = new DataTable("Moviesinfo");
            //returnVal.Fill(dt);
            //pb = returnVal;
            //dataGridView1.DataSource = dt;
   
        

        }
        public void Waiting()
        {
            int l=0;
            try
            {
                UpDateDataGrid("The server is running at port 8001...");
                UpDateDataGrid("The local End point is  :" + myList.LocalEndpoint);
                UpDateDataGrid("Waiting for a connection.....");
               //Socket s = myList.AcceptSocket();
                
                TcpClient cl = myList.AcceptTcpClient();
                NetworkStream ns = cl.GetStream();
                StreamWriter sw = new StreamWriter(ns);
                StreamReader sr = new StreamReader(sw.BaseStream);

                UpDateDataGrid("Connection accepted from: " + cl.Client.RemoteEndPoint); //UpDateDataGrid("Connection accepted from: " + s.RemoteEndPoint);
                while (true) 
                {
                    //byte[] b = new byte[100];
                    //s.Receive(b);
                    string rec = sr.ReadLine();
                    
                    UpDateDataGrid("\nRecieved...\n");
                    //rec = System.Text.Encoding.ASCII.GetString(b);
                    UpDateDataGrid(rec);
                   //rec= rec.Substring(0, 3);
                    if (rec.Equals("301"))
                    {
                        sw.WriteLine("201");
                        sw.Flush();
                        // s.Send(Encoding.ASCII.GetBytes("201"));
                    }
                    if (rec.Equals("311"))
                    {
                        sw.WriteLine(pb.ToString());
                        sw.Flush(); // s.Send(Encoding.ASCII.GetBytes(pb.ToString()));
                    }
                    if (rec.Equals("312"))
                    {
                        long offset = 0;
                        int buffersize = 972257;
                        
                        var fs = new FileStream(@"D:\sas.mp4", FileMode.Open);
                       
                            fs.Seek(offset, SeekOrigin.Begin);
                            // Read the file here 
                            byte[] data = new byte[buffersize];//[fs.Length];
                            int count = 0;
                            int sizeCount = 0;
                            while (fs.Read(data, 0, buffersize) > 0)
                            {
                                sizeCount += data.Length;
                                sw.Write(data);
                                //sw.WriteLine(Encoding.UTF8.GetString(data, 0, data.Length));
                                sw.Flush();
                               // s.Send(data);
                                count++;
                                data = new byte[buffersize];
                            }
                        sw.WriteLine("Done");//s.Send(Encoding.ASCII.GetBytes("Done"));
                            UpDateDataGrid(count.ToString());
                            UpDateDataGrid(sizeCount.ToString());
                            UpDateDataGrid("Done");
                            
                            
                    }
                    if (rec.Equals("310"))
                    {
                        sw.WriteLine("201"+Environment.NewLine+"Which type of categories to you want me to send?"+  Environment.NewLine+" 1 - Comedy 2 - Action 3 - Drama 4 - Horror\n");//  s.Send(Encoding.ASCII.GetBytes("201"+Environment.NewLine+"Which type of categories to you want me to send?"+  Environment.NewLine+" 1 - Comedy 2 - Action 3 - Drama 4 - Horror\n"));
                        //b = new byte[100];
                        rec = sr.ReadLine();
                       //  rec = System.Text.Encoding.ASCII.GetString(b);
                         //rec = rec.Substring(0, 1);
                         UpDateDataGrid("Client: " + rec);
                         if (rec.Equals("1"))
                         {
                             ////check comedy movies and send them to client

                         }
                         else if (rec.Equals("2"))
                         {
                             //check action movies and send them to client
                         }
                         else if (rec.Equals("3"))
                         {
                             //check drama movies and send them to client
                         }
                         else if (rec.Equals("4"))
                         {
                             //check horror movies and send them to client
                         }
                         else
                             sw.WriteLine("Wrong number of choise"); //s.Send(Encoding.ASCII.GetBytes("Wrong number of choise"));

                    }
                    else if (rec.Equals("500"))
                    {
                        sw.WriteLine("Disconnected"); //  s.Send(Encoding.ASCII.GetBytes("Disconnected"));
                        cl.Close();
                        l = 1;
                    }
                    //else
                      //  s.Send(b);
                    /* clean up */
                    //Console.ReadLine();
                }
                
            }
            catch (Exception e)
            {
                UpDateDataGrid("Error" + e.StackTrace.ToString());
            }

        }
        public void UpDateDataGrid(string displayString)
        {
            txtData.Invoke(new MethodInvoker(() => txtData.AppendText(Environment.NewLine+displayString)));
        }

        private void txtData_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
