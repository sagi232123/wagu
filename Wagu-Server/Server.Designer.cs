﻿namespace Wagu
{
    partial class Server
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbllog = new System.Windows.Forms.Label();
            this.txtData = new System.Windows.Forms.TextBox();
            this.lbllist = new System.Windows.Forms.Label();
            this.lblip = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // lbllog
            // 
            this.lbllog.AutoSize = true;
            this.lbllog.BackColor = System.Drawing.Color.Transparent;
            this.lbllog.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lbllog.ForeColor = System.Drawing.Color.White;
            this.lbllog.Location = new System.Drawing.Point(212, 25);
            this.lbllog.Name = "lbllog";
            this.lbllog.Size = new System.Drawing.Size(148, 25);
            this.lbllog.TabIndex = 0;
            this.lbllog.Text = "Wagu Server";
            // 
            // txtData
            // 
            this.txtData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtData.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.txtData.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.txtData.Location = new System.Drawing.Point(49, 141);
            this.txtData.Multiline = true;
            this.txtData.Name = "txtData";
            this.txtData.ReadOnly = true;
            this.txtData.Size = new System.Drawing.Size(459, 294);
            this.txtData.TabIndex = 1;
            this.txtData.TextChanged += new System.EventHandler(this.txtData_TextChanged);
            // 
            // lbllist
            // 
            this.lbllist.AutoSize = true;
            this.lbllist.BackColor = System.Drawing.Color.Transparent;
            this.lbllist.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lbllist.ForeColor = System.Drawing.Color.White;
            this.lbllist.Location = new System.Drawing.Point(44, 87);
            this.lbllist.Name = "lbllist";
            this.lbllist.Size = new System.Drawing.Size(144, 25);
            this.lbllist.TabIndex = 2;
            this.lbllist.Text = "Listening At:";
            // 
            // lblip
            // 
            this.lblip.AutoSize = true;
            this.lblip.BackColor = System.Drawing.Color.Transparent;
            this.lblip.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblip.ForeColor = System.Drawing.Color.White;
            this.lblip.Location = new System.Drawing.Point(194, 87);
            this.lblip.Name = "lblip";
            this.lblip.Size = new System.Drawing.Size(102, 25);
            this.lblip.TabIndex = 3;
            this.lblip.Text = "IP:PORT";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(158, 262);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(240, 150);
            this.dataGridView1.TabIndex = 5;
            // 
            // Server
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Orange;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(566, 455);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lblip);
            this.Controls.Add(this.lbllist);
            this.Controls.Add(this.txtData);
            this.Controls.Add(this.lbllog);
            this.Name = "Server";
            this.Text = "Server";
            this.Load += new System.EventHandler(this.Server_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbllog;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.Label lbllist;
        private System.Windows.Forms.Label lblip;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}

