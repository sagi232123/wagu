﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Windows.Forms;


namespace Wagu_Client
{
    public partial class Client : Form
    {
        public Client()
        {
            InitializeComponent();
        }
       private TcpClient tcpclnt;
       IPAddress ips = IPAddress.Parse("127.0.0.1");
        private void btnconn_Click(object sender, EventArgs e)
        {
            try
            {
                tcpclnt = new TcpClient();
               
                tcpclnt.Connect(ips, 8001);

                UpDateDataGrid("Connected");
        
            }
            catch (Exception pe)
            {
                UpDateDataGrid("Error" + pe.StackTrace.ToString());
            }
        }

        private void btnsnd_Click(object sender, EventArgs e)
        {
            String str = textBox1.Text;
            NetworkStream ns = tcpclnt.GetStream();
            String strr;
            //ASCIIEncoding asen = new ASCIIEncoding();
            StreamWriter sw = new StreamWriter(ns);
            StreamReader sr = new StreamReader(sw.BaseStream);
            //byte[] ba = asen.GetBytes(str);
            sw.WriteLine(str);  //    stm.Write(ba, 0, ba.Length);
            sw.Flush();
            int buffersize = 972257;
            txtData.AppendText(Environment.NewLine + "Server: " + sr.ReadLine());
            //byte[] bb = new byte[buffersize];
            FileStream fs = new FileStream(@"D:\new.mp4", FileMode.OpenOrCreate); //TODO: try catch!

            byte[] buffer = new byte[buffersize];
            sr.BaseStream.Read(buffer, 0, buffersize); // read the next chunk
            strr = Encoding.UTF8.GetString(buffer); // make sure the next chunk isn't EOF

            //strr = sr.ReadLine();
            strr = strr.Substring(0, 4);
            int count = 0;
            while (!strr.Contains("Done"))
            {
                //if (sr.Read() > 0)
                //{
                    /*String strr2 = strr.Replace(@"\0", "");
                    if (strr2.Length > 0)
                    {*/
                        byte[] bytes = new byte[strr.Length * sizeof(char)];
                        Buffer.BlockCopy(strr.ToCharArray(), 0, bytes, 0, bytes.Length);
                        //fs.Write(bb, 0, buffersize);
                        fs.Write(bytes, 0, bytes.Length); // write to file
                        sr.BaseStream.Read(buffer, 0, buffersize); // read the next chunk
                        strr = Encoding.UTF8.GetString(buffer); // make sure the next chunk isn't EOF
                        //asen.GetString(bb).ToString();
                        //strr = sr.ReadLine();
                        count++;
                        txtData.AppendText(Environment.NewLine + "Server: " + count);
                    //}
                //}
            }
            //txtData.AppendText(Environment.NewLine + "Server: " + strr);
            fs.Flush();
            fs.Close();
          
        }
        public void UpDateDataGrid(string displayString)
        {
            txtData.Invoke(new MethodInvoker(() => txtData.AppendText(displayString + "\n")));
        }

        private void Client_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
              
            Stream stm = tcpclnt.GetStream();
            string str="500";
            ASCIIEncoding asen = new ASCIIEncoding();
            byte[] ba = asen.GetBytes(str);
            stm.Write(ba, 0, ba.Length);
          tcpclnt.Close();
          Application.Exit();

            }
            catch (Exception ep)
            {

                MessageBox.Show("Not Connected\n"+ep.ToString());
                Application.Exit();
            }
            
        }

        private void Client_Load(object sender, EventArgs e)
        {

        }

        private void txtData_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var test = new MediaElementWindow();
            test.Show();
        }


    }
}
