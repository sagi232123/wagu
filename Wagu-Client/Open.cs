﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
namespace Wagu_Client
{
    public partial class Open : Form
    {
        int val = 0;
   
        public Open()
        {
            InitializeComponent();
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.BackColor = Color.Black;
            this.TransparencyKey = Color.Black;

            ModifyProgressBarColor.SetState(Loader, 3);
        }
  
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (val != 100)
            {
                val += 1;
                Loader.Value += 1;

            }
            else if(val==100)
            {

                timer1.Stop();
                this.Hide();
                Client cli = new Client();
                cli.Show();
        

            }
        }
    }
    public static class ModifyProgressBarColor
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr w, IntPtr l);
        public static void SetState(this ProgressBar pBar, int state)
        {
            SendMessage(pBar.Handle, 1040, (IntPtr)state, IntPtr.Zero);
        }
    }
}
