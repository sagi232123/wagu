﻿namespace Wagu_Client
{
    partial class Client
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnsnd = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtData = new System.Windows.Forms.TextBox();
            this.btnconn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnsnd
            // 
            this.btnsnd.Location = new System.Drawing.Point(352, 296);
            this.btnsnd.Name = "btnsnd";
            this.btnsnd.Size = new System.Drawing.Size(79, 23);
            this.btnsnd.TabIndex = 0;
            this.btnsnd.Text = "Send";
            this.btnsnd.UseVisualStyleBackColor = true;
            this.btnsnd.Click += new System.EventHandler(this.btnsnd_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 299);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(324, 20);
            this.textBox1.TabIndex = 1;
            // 
            // txtData
            // 
            this.txtData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtData.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.txtData.Location = new System.Drawing.Point(12, 12);
            this.txtData.Multiline = true;
            this.txtData.Name = "txtData";
            this.txtData.ReadOnly = true;
            this.txtData.Size = new System.Drawing.Size(398, 265);
            this.txtData.TabIndex = 2;
            this.txtData.TextChanged += new System.EventHandler(this.txtData_TextChanged);
            // 
            // btnconn
            // 
            this.btnconn.Location = new System.Drawing.Point(199, 327);
            this.btnconn.Name = "btnconn";
            this.btnconn.Size = new System.Drawing.Size(75, 23);
            this.btnconn.TabIndex = 3;
            this.btnconn.Text = "Connect";
            this.btnconn.UseVisualStyleBackColor = true;
            this.btnconn.Click += new System.EventHandler(this.btnconn_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(416, 224);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(46, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Orange;
            this.ClientSize = new System.Drawing.Size(474, 362);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnconn);
            this.Controls.Add(this.txtData);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnsnd);
            this.Name = "Client";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Client_FormClosing);
            this.Load += new System.EventHandler(this.Client_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnsnd;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.Button btnconn;
        private System.Windows.Forms.Button button1;
    }
}

